﻿CREATE TABLE [dbo].[Employee]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [AddressId] INT NOT NULL, 
    [PersonId] INT NOT NULL, 
    [CompanyName] NVARCHAR(20) NOT NULL, 
    [Position] NVARCHAR(30) NULL, 
    [EmployeeName] NVARCHAR(100) NULL, 
    CONSTRAINT [FK_Employee_Address] FOREIGN KEY (AddressId) REFERENCES [Address]([Id]), 
    CONSTRAINT [FK_Employee_Person] FOREIGN KEY ([PersonId]) REFERENCES [Person]([Id])
)

GO

CREATE TRIGGER Trigger_Insert
ON Employee
AFTER INSERT
AS
BEGIN
    INSERT INTO Company(
       Name,
	   AddressId
    )
	SELECT
       e.CompanyName,
	   e.AddressId
    FROM
        Employee e where e.Id=@@IDENTITY
END