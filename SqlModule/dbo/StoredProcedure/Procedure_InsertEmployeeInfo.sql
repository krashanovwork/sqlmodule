﻿CREATE PROCEDURE [dbo].[Procedure_InsertEmployeeInfo]

@EmployeeName nvarchar(100),
@FirstName nvarchar(50),
@LastName nvarchar(50),
@CompanyName nvarchar(20),
@Position nvarchar(50),
@Street nvarchar(50),
@City nvarchar(20),
@State nvarchar(50),
@ZipCode nvarchar(50)

AS
IF(@FirstName IS NULL) or (@LastName IS NULL) or (@EmployeeName IS NULL)
	BEGIN
		PRINT('Cannot be NULL')
		RETURN
	END
IF(@FirstName = '') or (@LastName = '') or (@EmployeeName = '')
	BEGIN
		PRINT('Cannot be EMPTY')
		RETURN
	END
IF LEN(@CompanyName) > 20
	BEGIN
		SET @CompanyName=SUBSTRING(@CompanyName, 1, 20)
	END

 DECLARE @AddressId INT;
 DECLARE @PersonId INT;

 INSERT INTO Address(Street, City, State, ZipCode) values(@Street, @City, @State, @ZipCode) SET @AddressId = @@IDENTITY;
 INSERT INTO Person(FirstName, LastName) values(@FirstName, @LastName) SET @PersonId = @@IDENTITY;

 INSERT INTO Employee(AddressId, PersonId, CompanyName, Position, EmployeeName) 
 VALUES(@AddressId, @PersonId, @CompanyName, @Position, @EmployeeName);