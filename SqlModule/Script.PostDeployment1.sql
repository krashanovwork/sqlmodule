﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

Insert into Person(FirstName, LastName) values ('Alex', 'Krashanov')

insert into Address(Street, City, State, Zipcode) values('Dobrovolskogo', 'HP', 'Kremenchuk', '228')


insert into Company(Name, AddressId) values('EPAM', 1)

insert into Employee(AddressId, PersonId, CompanyName, Position, EmployeeName) values(1, 1, 'EPAM', 'AQA', 'Oleksandr-Krashanov')